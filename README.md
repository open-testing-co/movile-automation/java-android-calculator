# Java Android Calculator

Android version 9.0
Design pattern POM or ScreenPlay
JAVA as the programming language
 4 basic calc basic operation (addition, subtraction, multiplication, and division) 
 
 
 
 ## Author ✒️
 
 * **Henry Andrés Correa Correa** - [Linkedin](https://www.linkedin.com/in/henryandrescorrea/) -  [h.andresc1127@gmai.com](mailto:h.andresc1127@gmai.com) \
 ![author](src/test/resources/img/author.png "Henry Andres Correa Correa")
 
 
 ### *[Hackathon2020-1](https://applitools.com/2020-cross-browser-testing-hackathon-winners/)* 🌟
 ![GoldWinner](src/test/resources/img/platinumWinner.jpg "Hackathon 2020-1")
 
 ### *[Hackathon2019](https://applitools.com/2019-ultrafast-visual-ai-hackathon-winners/)* 🌟
 ![GoldWinner](src/test/resources/img/goldWinner.jpg "Hackathon 2019")