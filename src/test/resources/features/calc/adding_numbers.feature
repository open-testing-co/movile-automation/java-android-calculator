#Author: Henry Andrés Correa Correa
Feature: Basic operations

  Background: starting driver
    Given start calculator

  Scenario: Adding two numbers
    Given a is "1"
    And b is "2"
    When I add a and b
    Then the total should be "3"

  Scenario Outline: Substracting
    Given a is <num1>
    And b is <num2>
    When I subtract a from b
    Then the total should be <total>

    Examples:
      | num1 | num2 | total |
      | "1"  | "2"  | "-1"  |
      | "3"  | "1"  | "2"   |
      | "-1" | "5"  | "-6"  |
      | "-5" | "2"  | "-7"  |

  Scenario: Multiplying two numbers
    Given a is "4.4"
    And b is "2"
    When I multiply a and b
    Then the total should be "8.8"

  Scenario: Dividing two numbers
    Given a is "4"
    And b is "2"
    When I divide a and b
    Then the total should be "2"