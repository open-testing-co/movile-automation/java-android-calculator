package com.handresc1127.acceptancetests;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/",
        glue = "com.handresc1127",
        tags = {""},
        plugin = {
                "pretty", "html:target/reports/html/",
                "junit:target/reports/junit.xml",
                "junit:target/reports/test.xml",
                "junit:target/reports/TEST-all.xml",
                "json:target/reports/cukes.json"
        }
)


public class AcceptanceTestSuite {
}
