package com.handresc1127.definitions;

import com.handresc1127.pages.Base;
import com.handresc1127.pages.CalculatorPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CalcStepdefs {
    @Given("start calculator")
    public void startCalculator() {
        Base.globalSetup();
        CalculatorPage.setUp();
    }

    @Given("a is {string}")
    public void aIs(String arg0) {
        CalculatorPage.operatorX(arg0);
    }

    @And("b is {string}")
    public void bIs(String arg0) {
        CalculatorPage.operatorY(arg0);
    }

    @Then("the total should be {string}")
    public void theTotalShouldBe(String arg0) {
        CalculatorPage.resultShouldBe(arg0);
    }

    @When("I add a and b")
    public void iAddAAndB() {
        CalculatorPage.XaddY();
    }

    @When("I subtract a from b")
    public void iSubtractAFromB() {
        CalculatorPage.XsubsY();
    }

    @When("I multiply a and b")
    public void iMultiplyAAndB() {
        CalculatorPage.XmulY();
    }

    @When("I divide a and b")
    public void iDivideAAndB() {
        CalculatorPage.XdivY();
    }

}
