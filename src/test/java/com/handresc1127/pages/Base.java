package com.handresc1127.pages;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.net.URL;

public abstract class Base {

    private static AppiumDriverLocalService appiumService;
    private static final String APPIUM_JS_PATH = System.getenv("AppData") + "/npm/node_modules/appium/build/lib/main.js";

    @BeforeSuite
    public static void globalSetup(){
        //Set Capabilities
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("noReset", "false");

        //Build the Appium service
        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withAppiumJS(new File(APPIUM_JS_PATH));
        builder.withIPAddress("127.0.0.1");
        builder.usingPort(4723);
        builder.withCapabilities(cap);
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL,"error");

        //Start the server with the builder
        appiumService = AppiumDriverLocalService.buildService(builder);
        appiumService.start();
    }

    @AfterSuite
    public static void globalTearDown(){
        if(appiumService!=null){
            appiumService.stop();
        }
    }

    public static URL getServiceUrl(){
        return appiumService.getUrl();
    }
}
