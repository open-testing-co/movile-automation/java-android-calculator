package com.handresc1127.pages;

import com.handresc1127.utilies.DevicePropertiesStore;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class CalculatorPage extends Base {

    private static AndroidDriver<WebElement> driver;
    private static String x, y;

    @BeforeClass
    public static void setUp() {

        DesiredCapabilities capabilities;
        capabilities = DevicePropertiesStore.getInstance("real").getAppiumCapabilities();
        capabilities.setCapability("appPackage", "com.android.calculator2");
        capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
        driver = new AndroidDriver<>(getServiceUrl(), capabilities);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    public static void resultShouldBe(String expectedResult) {
        String total = driver.findElementById("result").getText();
        int j = 0;
        char[] ch = new char[total.length()];
        for (int i = 0; i < total.length(); i++) {
            char aux = total.charAt(i);
            if (aux > 32) {
                if (aux < 125) {
                    ch[j] = total.charAt(i);
                } else {
                    ch[j] = (char) 45;
                }
                j++;
            }
        }
        String currentResult = String.valueOf(ch);
        Assert.assertEquals(currentResult, expectedResult);
    }

    public static void operatorX(String i) {
        x = i;
    }

    public static void operatorY(String i) {
        y = i;
    }

    public static void XaddY() {
        calcType(x);
        driver.findElementByAccessibilityId("plus").click();
        calcType(y);
    }

    public static void XsubsY() {
        calcType(x);
        driver.findElementByAccessibilityId("minus").click();
        calcType(y);
    }

    public static void XmulY() {
        calcType(x);
        driver.findElementById("op_mul").click();
        calcType(y);
    }

    public static void XdivY() {
        calcType(x);
        driver.findElementByAccessibilityId("divide").click();
        calcType(y);
    }

    public static void tapOnCalElement(char ch) {
        String target = "digit_" + ch;
        if (ch == '-') target = "op_sub";
        else if (ch == '.') target = "dec_point";
        driver.findElementById(target).click();
    }

    public static void calcType(String data) {
        for (char ch : data.toCharArray()) {
            tapOnCalElement(ch);
        }
    }

}
