package com.handresc1127.utilies;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Set;

public class DevicePropertiesStore {

    private static final String DEVICE_IMAGE_PATH_CAPABILITY_KEY = "image.file";
    private static final String PROPERTIES_FILE_EXTENSION = ".conf";
    private static final String REAL_DEVICE_PROPERTIES_LOCATION = "lenovo";
    private static final String EMULATOR_DEVICE_PROPERTIES_LOCATION = "emulator";
    private static final String CLOUD_DEVICE_PROPERTIES_LOCATION = "lenovo";


    private final Properties deviceProperties = new Properties();
    private static final String DEVICES_FILES_PATH = "/src/test/resources/devices";


    private DevicePropertiesStore(String device) {
        try {
            InputStream in = new FileInputStream(getDeviceFilePath(device));
            deviceProperties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getDeviceFilePath(String desiredDevice) {
        Path deviceFilePath = Paths.get(getCurrentWorkingDirectory(), DEVICES_FILES_PATH);
        switch (desiredDevice) {
            case "emulator":
                deviceFilePath = Paths.get(String.valueOf(deviceFilePath),
                        EMULATOR_DEVICE_PROPERTIES_LOCATION + PROPERTIES_FILE_EXTENSION);
                break;
            case "cloud":
                deviceFilePath = Paths.get(String.valueOf(deviceFilePath),
                        CLOUD_DEVICE_PROPERTIES_LOCATION + PROPERTIES_FILE_EXTENSION);
                break;
            default:
                deviceFilePath = Paths.get(String.valueOf(deviceFilePath),
                        REAL_DEVICE_PROPERTIES_LOCATION + PROPERTIES_FILE_EXTENSION);
                break;
        }
        return String.valueOf(deviceFilePath);
    }

    private static String getCurrentWorkingDirectory() {
        return System.getProperty("user.dir");
    }

    public static DevicePropertiesStore getInstance(String device) {
        return new DevicePropertiesStore(device);
    }

    public String getProperty(String key) {
        return deviceProperties.getProperty(key);
    }

    public DesiredCapabilities getAppiumCapabilities() {
        DesiredCapabilities appiumCapabilities = new DesiredCapabilities();

        Set<String> desiredCapabilityNames = deviceProperties.stringPropertyNames();

        desiredCapabilityNames
                .forEach(desiredCapability -> {
                    if (!desiredCapability.equalsIgnoreCase(DEVICE_IMAGE_PATH_CAPABILITY_KEY)) {
                        appiumCapabilities.setCapability(desiredCapability, getProperty(desiredCapability));
                    }
                });

        return appiumCapabilities;
    }
}